﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(eYikat.Startup))]
namespace eYikat
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
