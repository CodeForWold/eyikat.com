﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eYikat.Areas.Admin.Models
{
    public class IlanlarModel
    {
        public int ID { get; set; }
        public string SirketAdi { get; set; }
        public string Sehir { get; set; }
        public string Ilce { get; set; }
        public string kisaYazi { get; set; }
        public Nullable<int> fiyat { get; set; }
        public HttpPostedFileBase Resim { get; set; }

    }
}