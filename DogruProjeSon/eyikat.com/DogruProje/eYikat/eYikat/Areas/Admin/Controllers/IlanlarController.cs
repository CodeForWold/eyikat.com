﻿using eYikat.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eYikat.Areas.Admin.Controllers
{
    public class IlanlarController : Controller
    {
        eYikatEntities2 db = new eYikatEntities2();
        // GET: Admin/Ilanlar
        public ActionResult Index()
        {
            var model =  db.Ilanlar.OrderByDescending(x => x.ID).ToList();
            return View(model);
        }

        public ActionResult Add()
        {
            
            return View();
        }

        const string imageFolderPath = "/Content/ilanlar-images/";
        public ActionResult AddIlan(IlanlarModel model)
        {
            if (ModelState.IsValid)
            {
                string fileName = string.Empty;
                //Dosya kaydetme
                if (model.Resim != null && model.Resim.ContentLength > 0)
                {
                    fileName = model.Resim.FileName;
                    var path = Path.Combine(Server.MapPath("~" + imageFolderPath), fileName);
                    model.Resim.SaveAs(path);
                }

                Ilanlar ilanlar = new Ilanlar();
                ilanlar.fiyat = model.fiyat;
                ilanlar.Ilce = model.Ilce;
                ilanlar.kisaYazi = model.kisaYazi;
                ilanlar.Sehir = model.Sehir;
                ilanlar.SirketAdi = model.SirketAdi;
                ilanlar.ResimYolu = imageFolderPath + fileName;

                db.Ilanlar.Add(ilanlar);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = db.Ilanlar.Find(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Ilanlar model)
        {
            if (ModelState.IsValid)
            {
                db.Ilanlar.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {

            var model = db.Ilanlar.Find(id);

            return View(model);
        }

        [HttpGet]
        public ActionResult DeleteConfirm(int id)
        {

            var model = db.Ilanlar.Find(id);
            db.Ilanlar.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}