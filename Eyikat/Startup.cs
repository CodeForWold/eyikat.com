﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Eyikat.Startup))]
namespace Eyikat
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
