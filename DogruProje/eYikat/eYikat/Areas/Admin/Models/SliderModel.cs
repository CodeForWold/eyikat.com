﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eYikat.Areas.Admin.Models
{
    public class SliderModel
    {
        public int ID { get; set; }
        public string SliderText { get; set; }
        public HttpPostedFileBase SliderFoto { get; set; }
    }
}