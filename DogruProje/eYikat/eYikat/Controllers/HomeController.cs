﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eYikat.Controllers
{


    public class HomeController : Controller
    {
        eYikatEntities2 db = new eYikatEntities2();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult urunler()
        {
            Models.UrunlerModel dto = new Models.UrunlerModel();
            dto.Ilanlar = db.Ilanlar.ToList();
            return View(dto);
        }

        public ActionResult urun()
        {
            return View();
        }
        public ActionResult ilanver()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult _Slider()
        {
            return View(db.Slider);
        }

    }
}